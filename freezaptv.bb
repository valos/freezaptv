DESCRIPTION = "An application to turn your mobile device into a Freebox HD remote"
HOMEPAGE = "http://code.google.com/p/freezaptv/"
LICENSE = "GPLv3"
AUTHOR = "Valéry Febvre <vfebvre@easter-eggs.com>"
SECTION = "x11/applications"
PRIORITY = "optional"

SRCREV = "2"
PV = "1.0.0+svnr${SRCPV}"

PACKAGE_ARCH = "all"

SRC_URI = "svn://freezaptv.googlecode.com/svn;module=trunk;proto=http"
S = "${WORKDIR}/trunk"

inherit distutils

FILES_${PN} += "${datadir}/freezaptv ${datadir}/applications/freezaptv.desktop ${datadir}/pixmaps/freezaptv.png"

RDEPENDS += "python-elementary"

do_compile_prepend() {
	${STAGING_BINDIR_NATIVE}/edje_cc -id ${S}/data ${S}/data/freezaptv.edc
}

#!/bin/sh

rm -rf tmp

cd ..
rm -rf build
python setup.py bdist
cd pkg

mkdir tmp
mkdir tmp/CONTROL
cp control tmp/CONTROL

# tar contents into pkg dir
tar -C tmp -xvzf ../dist/freezaptv*i686.tar.gz

chown -R root:root tmp

mv tmp/usr/lib/python2.5/ tmp/usr/lib/python2.6

# make ipk
./ipkg-build tmp

rm -rf tmp

     _____              _____         _______     __
    |  ___| __ ___  ___|__  /__ _ _ _|_   _\ \   / /
    | |_ | '__/ _ \/ _ \ / // _` | '_ \| |  \ \ / /
    |  _|| | |  __/  __// /| (_| | |_) | |   \ V /  
    |_|  |_|  \___|\___/____\__,_| .__/|_|    \_/
                                 |_|
Description
===========
FreeZapTV turns your mobile device into a Freebox HD remote (it was
specialy written for the mobile phone Openmoko Neo Freerunner).

https://gitlab.com/valos/freezaptv

Licensed under the GNU General Public License v3.


Authors
=======
Valéry Febvre <vfebvre@easter-eggs.com>


Requirements
============
- python-elementary


Notes
=====
- FreeZapTV is tested on SHR unstable only.
  Normally, it should run on any system with a revision of
  python-elementary equal or greater to 40756.
- FreeZapTV base home directory is $HOME/.config/freezaptv.
  This directory and related files are created after first run.


Report Bugs
===========
If there is something wrong, please use to the Google code issues:
https://gitlab.com/valos/freezaptv/issues
Report a bug or look there for possible workarounds.


Credits
=======
- Suzanna Smith and Calum Benson for High Contrast icons


Version History
===============

2010-05-18 - Version 1.0.0
--------------------------
- First release.

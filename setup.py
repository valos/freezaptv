# -*- coding: utf-8 -*-

# FreeZapTV -- An application to turn your mobile device into a Freebox HD
# remote
#
# Copyright (C) 2010 Valéry Febvre <vfebvre@easter-eggs.com>
# http://code.google.com/p/freezaptv/
#
# This file is part of FreeZapTV.
#
# FreeZapTV is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# FreeZapTV is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from distutils.core import setup

def get_images_list():
    l = []
    for root, dirs, files in os.walk('data/images/', topdown = False):
        if root != 'data/images/': continue
        for name in files:
            if name in ('remote.png',):
                continue
            l.append(os.path.join(root, name))
    return l

def main():
    setup(name         = 'freezaptv',
          version      = '1.0.0',
          description  = 'An application to turn your phone into a Freebox HD remote',
          author       = 'Valery Febvre',
          author_email = 'vfebvre@easter-eggs.com',
          url          = 'http://code.google.com/p/freezaptv/',
          classifiers  = [
            'Development Status :: 5 - Production/Stable',
            'Environment :: X11 Applications',
            'Intended Audience :: End Users/Phone UI',
            'License :: GNU General Public License (GPL)',
            'Operating System :: POSIX',
            'Programming Language :: Python',
            'Topic :: Desktop Environment',
            ],
          packages     = ['freezaptv'],
          scripts      = ['freezaptv/freezaptv'],
          data_files   = [
            ('share/applications', ['data/freezaptv.desktop']),
            ('share/pixmaps', ['data/freezaptv.png']),
            ('share/freezaptv', ['README', 'data/freezaptv.edj']),
            ('share/freezaptv/images', get_images_list()),
            ],
          )

if __name__ == '__main__':
    main()
